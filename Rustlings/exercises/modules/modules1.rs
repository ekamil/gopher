// modules1.rs
// Make me compile! Execute `rustlings hint modules1` for hints :)

mod sausage_factory {
    pub fn make_sausage() {
        println!("{}", SAUSAGE.to_owned());
    }
    const SAUSAGE: &str = "sausage!";
}

fn main() {
    sausage_factory::make_sausage();
}
