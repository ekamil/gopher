package bob

import (
	"regexp"
	"strings"
)

// Hey - simulates a lackadaisical interlocutor
func Hey(remark string) string {
	var onlySilence, _ = regexp.MatchString(`^\s+$`, remark)
	onlySilence = len(remark) == 0 || onlySilence
	hasLetters, _ := regexp.MatchString(`[A-Za-z]+`, remark)
	var isQ = strings.HasSuffix(strings.TrimSpace(remark), "?")
	var isUpper = remark == strings.ToUpper(remark) && hasLetters
	if onlySilence {
		return "Fine. Be that way!"
	} else if isUpper && isQ {
		return "Calm down, I know what I'm doing!"
	} else if isUpper {
		return "Whoa, chill out!"
	} else if isQ {
		return "Sure."
	} else {
		return "Whatever."
	}
}
