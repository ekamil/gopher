package hamming

import "fmt"

func Distance(a, b string) (int, error) {
	var lenA, lenB = len(a), len(b)
	if lenA != lenB {
		return -1, IncompatibleLengthsError{
			a: lenA,
			b: lenB,
		}
	}
	var diffs = 0
	runesB := []rune(b)
	for idx, letter := range a {
		other := runesB[idx]
		if other != letter {
			diffs++
		}
	}
	return diffs, nil
}

// IncompatibleLengthsError Strings passed to the Distance method were not same-length
type IncompatibleLengthsError struct {
	a int
	b int
}

func (f IncompatibleLengthsError) Error() string {
	return fmt.Sprintf("Strings aren't same length. Got %d and %d", f.a, f.b)
}
