package gigasecond

import "time"
import "math"

// AddGigasecond - adds a billion seconds to a given time
func AddGigasecond(t time.Time) time.Time {
	var giga = math.Pow(10, 9)
	var gigaSecondDuration = time.Duration(giga) * time.Second
	var result = t.Add(gigaSecondDuration)
	return result
}
