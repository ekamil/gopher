package isogram

import (
	"unicode"
)

// IsIsogram checks if the word doesn't contain repeated letters
func IsIsogram(i string) bool {
	var letters = make(map[rune]int)
	for _, char := range i {
		if unicode.IsLetter(char) {
			char = unicode.ToUpper(char)
			var _, exists = letters[char]
			if exists {
				// letters[char]++ // that's legal
				return false
			}
			letters[char] = 1
		}
	}
	return true
}
