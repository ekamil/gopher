package diffsquares

import "math"

func SquareOfSum(n int64) int64 {
	var runningSum int64 = 0
	for i := int64(1); i <= n; i++ {
		runningSum += i
	}
	return runningSum * runningSum
}

func SumOfSquares(n int64) int64 {
	var runningSum int64 = 0
	for i := int64(1); i <= n; i++ {
		runningSum += i * i
	}
	return runningSum
}

func Difference(n int64) float64 {
	return math.Abs(float64(SumOfSquares(n) - SquareOfSum(n)))
}
