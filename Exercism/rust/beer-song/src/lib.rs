// use std::ops::Add;

fn bottles_descr(n: u32, title: bool) -> String {
    return if n == 0 {
        let t = if title {
            "No".to_string()
        } else {
            "no".to_string()
        };
        format!("{} more bottles of beer", t)
    } else if n == 1 {
        format!("{} bottle of beer", n)
    } else {
        format!("{} bottles of beer", n)
    };
}
pub fn verse(n: u32) -> String {
    let bottles_now = n;
    let bottles_after = if bottles_now > 0 { bottles_now - 1 } else { 99 };
    let cta: String = if bottles_now == 1 {
        "Take it down and pass it around".to_string()
    } else if bottles_now > 1 {
        "Take one down and pass it around".to_string()
    } else {
        "Go to the store and buy some more".to_string()
    };
    return format!(
        "{now_start} on the wall, {now}.\n{cta}, {left} on the wall.\n",
        now_start = bottles_descr(bottles_now, true),
        now = bottles_descr(bottles_now, false),
        cta = cta,
        left = bottles_descr(bottles_after, false)
    );
}

pub fn sing(start: u32, end: u32) -> String {
    let mut res: Vec<String> = vec![];
    for v in (end..=start).rev() {
        res.push(verse(v));
    }
    return res.join("\n");
}
