pub fn is_leap_year(year: u64) -> bool {
    let by4: bool = year % 4 == 0;
    let by100: bool = year % 100 == 0;
    let by400: bool = year % 400 == 0;
    by400 || (by4 && !by100)
}
