module HelloWorld (hello) where

import Data.Maybe (Maybe (Nothing))

hello :: String
hello = "Hello, World!"

sendEmail :: String -> Maybe String
sendEmail x = do
  Nothing

data Email = Email String

sendEmail' :: Email -> Maybe String
sendEmail' x = do
  Nothing
