module SpaceAge (Planet (..), ageOn) where

data Planet
  = Mercury
  | Venus
  | Earth
  | Mars
  | Jupiter
  | Saturn
  | Uranus
  | Neptune

earthsOrbitalPeriods :: Planet -> Float
earthsOrbitalPeriods Mercury = 0.2408467
earthsOrbitalPeriods Venus = 0.61519726
earthsOrbitalPeriods Earth = 1.0
earthsOrbitalPeriods Mars = 1.8808158
earthsOrbitalPeriods Jupiter = 11.862615
earthsOrbitalPeriods Saturn = 29.447498
earthsOrbitalPeriods Uranus = 84.016846
earthsOrbitalPeriods Neptune = 164.79132

earthYearInSeconds :: Float
earthYearInSeconds = 31557600

ageOn :: Planet -> Float -> Float
ageOn planet seconds = (seconds / earthYearInSeconds) / earthsOrbitalPeriods planet
-- alternatywne rozwiązanie z exercism
ageOn planet seconds = seconds / (31557600 * coefficient)
  where
    coefficient = case planet of
      Mercury -> 0.2408467
      Venus -> 0.61519726
      Earth -> 1
      Mars -> 1.8808158
      Jupiter -> 11.862615
      Saturn -> 29.447498
      Uranus -> 84.016846
      Neptune -> 164.79132