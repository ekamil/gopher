module Anagram (anagramsFor) where

import Data.Char (toUpper)

anagramsFor :: String -> [String] -> [String]
anagramsFor subject candidates = filter (\candidate -> isAnagram subject candidate) candidates

isAnagram :: String -> String -> Bool
isAnagram [] [] = False
isAnagram a b = length a == length b && corresponds && not areSame
  where
    uppercasedA = fmap toUpper a
    uppercasedB = fmap toUpper b
    corresponds = hasAllLetters uppercasedA uppercasedB
    areSame = uppercasedA == uppercasedB

hasAllLetters :: String -> String -> Bool
hasAllLetters as bs = "" == charDiff as bs

charDiff :: (Eq a) => [a] -> [a] -> [a]
charDiff as bs = foldl (\acc c -> popAt' c acc) bs as
  where
    popAt' = \aChar aString -> popAt (pos1 aChar aString) aString

pos1 :: (Eq a) => a -> [a] -> Int
pos1 _ [] = -1
pos1 a s = length $ takeWhile (\c -> c /= a) s

popAt :: (Eq a) => Int -> [a] -> [a]
popAt (-1) s = s
popAt _ [] = []
popAt idx s = fst parts ++ latter
  where
    parts = splitAt idx s
    second_part = snd parts
    latter = if (length second_part > 0) then tail second_part else []

-- |
-- let as = "listens"
-- let bs = "sinlets"
-- let a = as !! 0
-- popAt a bs
