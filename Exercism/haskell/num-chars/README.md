# Num Chars

## Non exercism
This is from cassidoo email not from exercism, but the structure looks similar

## Task
Given a string s and a character c, return the number of occurrences of c in s.

```
$ numChars(‘oh heavens’, ‘h’)
$ 2
```
