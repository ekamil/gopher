{-# OPTIONS_GHC -Wincomplete-patterns #-}

module NumChars (numChars, numChars') where

numChars :: String -> Char -> Int
numChars "" _ = 0
numChars (hay : []) needle = if needle == hay then 1 else 0
numChars (hay : stack) needle = (numChars [hay] needle) + (numChars stack needle)

numChars' :: Char -> String -> Int
numChars' needle haystack = foldl (\cnt x -> cnt + if needle == x then 1 else 0) 0 haystack

{--
Accumulator is `numChars [hay] needle`?
list is the rest of the string?
--}