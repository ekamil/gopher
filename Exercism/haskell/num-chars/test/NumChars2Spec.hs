module NumChars2Spec (spec) where

import Data.Foldable (for_)
import Data.Function (on)
import NumChars (numChars')
import Test.Hspec (Spec, describe, it, shouldBe)
import Test.Hspec.Runner (configFastFail, defaultConfig, hspecWith)

spec :: Spec
spec = describe "numChars'" $ do
  it "can handle empty string" $ do
    numChars' 'f' "" `shouldBe` 0
  it "counts to zero" $ do
    numChars' 'a' "AAA" `shouldBe` 0
  it "handles case 1" $ do
    numChars' 'A' "aAa" `shouldBe` 1
  it "handles case 2" $ do
    numChars' 'a' "aAa" `shouldBe` 2
  it "unicode" $ do
    numChars' 'ą' "ęąśćź" `shouldBe` 1