{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}

module NumCharsSpec (spec) where

import Data.Foldable (for_)
import Data.Function (on)
import NumChars (numChars)
import Test.Hspec (Spec, describe, it, shouldBe)
import Test.Hspec.Runner (configFastFail, defaultConfig, hspecWith)

-- main :: IO ()
-- main = hspecWith defaultConfig {configFastFail = False} spec

spec :: Spec
spec = describe "numChars" $ do
  it "can handle empty string" $ do
    numChars "" 'f' `shouldBe` 0
  it "counts to zero" $ do
    numChars "AAA" 'a' `shouldBe` 0
  it "handles case 1" $ do
    numChars "aAa" 'A' `shouldBe` 1
  it "handles case 2" $ do
    numChars "aAa" 'a' `shouldBe` 2
  it "unicode" $ do
    numChars "ęąśćź" 'ą' `shouldBe` 1
