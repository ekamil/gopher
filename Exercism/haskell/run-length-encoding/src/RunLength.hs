module RunLength (decode, encode) where

import Data.Char (digitToInt, isDigit, isLetter)
import Text.Printf

decode :: String -> String
decode [] = []
decode encodedText = decodeR encodedText 0

decodeR :: [Char] -> Int -> String
decodeR [] _ = []
decodeR txt@(x : xs) 0
  | isDigit x = decodeR xs times
  | isLetter x = decodeR txt 1
  | otherwise = [x] ++ (decodeR xs 0)
  where
    times = digitToInt x
decodeR (x : xs) n
  | isDigit x =
    decodeR
      xs
      newTimes
  | otherwise =
    (replicate n x)
      ++ (decodeR xs 0)
  where
    times = digitToInt x
    newTimes = 10 * n + times

encode :: String -> String
encode text = encodeR text 0

encodeR :: String -> Int -> String
encodeR [] _ = []
encodeR [x] n
  | n > 0 = printf "%d%v" (succ n) x
  | otherwise = [x]
encodeR (x : y : xs) n
  | x == y = "" ++ encodeR (y : xs) (n + 1)
  | n > 0 = printf "%d%v" (n + 1) x ++ encodeR (y : xs) 0
  | otherwise = [x] ++ encodeR (y : xs) n