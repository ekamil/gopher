module Grains (square, total) where

square :: Integer -> Maybe Integer
square n
  | n < 1 = Nothing
  | n <= 64 = Just $ 2 ^ pred n
  | n > 64 = Nothing

total :: Integer
-- total = sum [2 ^ x | x <- [0 .. 63]]
total = sum $ fmap (\x -> 2 ^ x) [0 .. 63]
