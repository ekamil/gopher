module LeapYear (isLeapYear) where

isLeapYear :: Integer -> Bool
isLeapYear year
  | isDivBy4 year && not (isDivBy100 year) = True
  | isDivBy400 year = True
  | otherwise = False

(%) :: (Integral a) => a -> a -> a
0 % _ = 0
_ % 0 = 0
a % b = a - b * (a `div` b)

isDivBy4 :: Integer -> Bool
isDivBy4 n = n % 4 == 0

isDivBy100 :: Integer -> Bool
isDivBy100 n = n % 100 == 0

isDivBy400 :: Integer -> Bool
isDivBy400 n = n % 400 == 0