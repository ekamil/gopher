{-# LANGUAGE RecordWildCards #-}

import Data.Foldable (for_)
import qualified IsbnVerifier
import Test.Hspec (Spec, describe, it, shouldBe)
import Test.Hspec.Runner (configFastFail, defaultConfig, hspecWith)

main :: IO ()
main = hspecWith defaultConfig {configFastFail = False} specs

specs :: Spec
specs = do
  describe "asNums" $ do
    it "should parse a number" $
      IsbnVerifier.asNums "1234" `shouldBe` [1, 2, 3, 4]
    it "should parse an isbn" $
      IsbnVerifier.asNums "598-21507-X" `shouldBe` [5, 9, 8, 2, 1, 5, 0, 7, 10]
    it "should ignore spaces" $
      IsbnVerifier.asNums "598-21507-X " `shouldBe` [5, 9, 8, 2, 1, 5, 0, 7, 10]
    it "handles isbn-13" $
      IsbnVerifier.asNums "978-3-16-148410-0" `shouldBe` [9, 7, 8, 3, 1, 6, 1, 4, 8, 4, 1, 0, 0]
  describe "looksLikeISBN10" $ do
    it "should recognize valid isbn" $
      IsbnVerifier.looksLikeISBN10 [3, 5, 9, 8, 2, 1, 5, 0, 8, 8] `shouldBe` True
    it "should accept isbn with X(10) only on the last position" $
      IsbnVerifier.looksLikeISBN10 [3, 5, 9, 8, 2, 1, 5, 0, 8, 10] `shouldBe` True
    it "should reject isbn with X(10) not on the last position" $
      IsbnVerifier.looksLikeISBN10 [10, 5, 9, 8, 2, 1, 5, 0, 8, 8] `shouldBe` False
    it "should reject too short input" $
      IsbnVerifier.looksLikeISBN10 [5, 9, 8] `shouldBe` False
    it "should reject too long input" $
      IsbnVerifier.looksLikeISBN10 [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12] `shouldBe` False
  describe "checksumForISBN10" $ do
    it "should get 0" $ IsbnVerifier.checksumForISBN10 [3, 5, 9, 8, 2, 1, 5, 0, 8, 8] `shouldBe` 0
    it "should get 3" $ IsbnVerifier.checksumForISBN10 [3, 5, 9, 8, 2, 1, 5, 0, 8, 0] `shouldBe` 3
    it "should get 11" $ IsbnVerifier.checksumForISBN10 [1, 2, 3, 4, 5, 6, 7, 8, 9, 0] `shouldBe` 1
  describe "isbn" $ for_ cases test
  where
    test Case {..} = do
      it description $ IsbnVerifier.isbn input `shouldBe` expected

data Case = Case
  { description :: String,
    input :: String,
    expected :: Bool
  }

cases :: [Case]
cases =
  [ Case
      { description = "valid isbn number",
        input = "3-598-21508-8",
        expected = True
      },
    Case
      { description = "invalid isbn check digit",
        input = "3-598-21508-9",
        expected = False
      },
    Case
      { description = "valid isbn number with a check digit of 10",
        input = "3-598-21507-X",
        expected = True
      },
    Case
      { description = "check digit is a character other than X",
        input = "3-598-21507-A",
        expected = False
      },
    Case
      { description = "invalid character in isbn",
        input = "3-598-P1581-X",
        expected = False
      },
    Case
      { description = "X is only valid as a check digit",
        input = "3-598-2X507-9",
        expected = False
      },
    Case
      { description = "valid isbn without separating dashes",
        input = "3598215088",
        expected = True
      },
    Case
      { description = "isbn without separating dashes and X as check digit",
        input = "359821507",
        expected = False
      },
    Case
      { description = "too long isbn and no dashes",
        input = "3598215078X",
        expected = False
      },
    Case
      { description = "too short isbn",
        input = "00",
        expected = False
      },
    Case
      { description = "isbn without check digit",
        input = "3-598-21507",
        expected = False
      },
    Case
      { description = "check digit of X should not be used for 0",
        input = "3-598-21515-X",
        expected = False
      },
    Case
      { description = "empty isbn",
        input = "",
        expected = False
      },
    Case
      { description = "input is 9 characters",
        input = "134456729",
        expected = False
      },
    Case
      { description = "invalid characters are not ignored",
        input = "3132P34035",
        expected = False
      },
    Case
      { description = "input is too long but contains a valid isbn",
        input = "98245726788",
        expected = False
      },
    Case
      { description = "isbn13 - a valid check digit",
        input = "978-0-306-40615-7",
        expected = True
      },
    Case
      { description = "isbn13 - an invalid check digit",
        input = "978-0-306-40615-0",
        expected = False
      }
  ]
