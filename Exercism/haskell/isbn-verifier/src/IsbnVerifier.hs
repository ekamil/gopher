{-# OPTIONS_GHC -Wall #-}

module IsbnVerifier (isbn, asNums, looksLikeISBN10, checksumForISBN10) where

isbn :: String -> Bool
isbn input
  | looksLikeISBN10 ns = checksumForISBN10 ns == 0
  | looksLikeISBN13 ns = checksumForISBN13 ns == 0
  | otherwise = False
  where
    ns = asNums input

checksumForISBN10 :: [Integer] -> Integer
checksumForISBN10 ns = mod (sum $ fmap mod11 (factors ns)) 11
  where
    weights = [11 - x | x <- [1 .. 10]] :: [Integer]
    factors = \x -> zip x weights
    mod11 = \(n, w) -> (n * w) `mod` 11

checksumForISBN13 :: [Integer] -> Integer
checksumForISBN13 ns = mod (sum $ fmap summing (factors ns)) 10
  where
    weights = take 13 $ cycle [1, 3]
    factors = \x -> zip x weights
    summing = \(n, w) -> (n * w)

looksLikeISBN10 :: [Integer] -> Bool
looksLikeISBN10 [] = False
looksLikeISBN10 ns = hasLen10 ns && noXinside ns
  where
    hasLen10 = \x -> length x == 10
    noXinside = \x -> not $ elem 10 $ take 9 x

looksLikeISBN13 :: [Integer] -> Bool
looksLikeISBN13 [] = False
looksLikeISBN13 ns = hasLen13 ns && noXinside ns
  where
    hasLen13 = \x -> length x == 13
    noXinside = \x -> not $ elem 10 x

asNums :: String -> [Integer]
asNums [x] = case x of
  '0' -> [0]
  '1' -> [1]
  '2' -> [2]
  '3' -> [3]
  '4' -> [4]
  '5' -> [5]
  '6' -> [6]
  '7' -> [7]
  '8' -> [8]
  '9' -> [9]
  'X' -> [10]
  _ -> []
asNums (x : xs) = asNums [x] ++ asNums xs
asNums [] = []