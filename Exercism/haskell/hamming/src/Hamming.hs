module Hamming (distance) where

distance :: String -> String -> Maybe Int
distance (_ : _) [] = Nothing
distance [] (_ : _) = Nothing
distance [] [] = Just 0
distance (x : []) (y : []) = if x == y then Just 0 else Just 1
distance (x : xs) (y : ys) = sum' (distance [x] [y]) (distance xs ys)

sum' :: Maybe Int -> Maybe Int -> Maybe Int
sum' (Just x) (Just y) = Just (x + y)
sum' _ _ = Nothing