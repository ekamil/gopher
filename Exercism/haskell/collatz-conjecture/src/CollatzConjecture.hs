module CollatzConjecture (collatz, collatz2, collatz3) where

collatz :: Integer -> Maybe Integer
collatz number
  | number <= 0 = Nothing
  | number == 1 = Just 0
  | otherwise = Just steps
  where
    (_, steps) = collatzFold (Just number) (0 :: Integer)

collatzFold :: Maybe Integer -> Integer -> (Maybe Integer, Integer)
collatzFold _ 200 = (Nothing, 200)
collatzFold (Just 1) steps = (Just 1, steps)
collatzFold Nothing steps = (Nothing, steps)
collatzFold (Just number) steps
  | number < 1 = (Nothing, 0)
  | otherwise = collatzFold (Just nextNumber) nextStep
  where
    nextStep = steps + 1
    nextNumber = if even number then number `div` 2 else number * 3 + 1

-- https://exercism.io/tracks/haskell/exercises/collatz-conjecture/solutions/2475564046ff4125b64206991b2dfde5
collatz2 :: Integer -> Maybe Integer
collatz2 n
  | n <= 0 = Nothing
  | n == 1 = Just 0
  | even n = succ <$> collatz2 (n `div` 2)
  | otherwise = succ <$> collatz2 (3 * n + 1)

-- https://exercism.io/tracks/haskell/exercises/collatz-conjecture/solutions/446fb65bc4d840f1a44ec0fe00bcbf3c
collatz3 :: Integer -> Maybe Integer
collatz3 n
  | n <= 0 = Nothing
  | n == 1 = Just 0
  | mod n 2 == 0 = fmap (+ 1) $ collatz3 (n `div` 2)
  | otherwise = fmap (+ 1) $ collatz3 (n * 3 + 1)