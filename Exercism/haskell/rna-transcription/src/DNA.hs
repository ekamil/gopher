module DNA (toRNA) where

import Data.Either (partitionEithers)

toRNA :: String -> Either Char String
toRNA dna = if hasError then Left (head bad) else Right (snd almostRNA)
  where
    almostRNA = partitionEithers (map transcribe dna)
    bad = fst almostRNA
    hasError = length bad > 0

transcribe :: Char -> Either Char Char
transcribe 'G' = Right 'C'
transcribe 'C' = Right 'G'
transcribe 'T' = Right 'A'
transcribe 'A' = Right 'U'
transcribe x = Left x